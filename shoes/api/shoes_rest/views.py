from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Shoe, BinVO

# Create your views here.

class BinVODetailEncoder(ModelEncoder):
        model = BinVO
        properties = ["closet_name", "import_href"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "id", "bin"]

    encoders = {
        "bin": BinVODetailEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":

        shoes = Shoe.objects.all()

        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
    #first line after try constructs a url basedo n the bin_vo_id.
    #second line retrieves an object from insomnia if it matches the bin_href that was generated previously
    #third line assigns an object content["bin"] if a matching object is found
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status = 400,
            )
    # note first line creates a new shoe object and saves it in the database. ** content unpacks the dictionary
    # that is initialized above. safe = False allows json to serialize non-dict objects (because i think it only accepts dictionaries)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"})
