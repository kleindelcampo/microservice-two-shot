from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse

# Create your models here.
#manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe where it exists.

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

class Shoe(models.Model):
    name = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})
